package controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import model.Funcionario;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@RequestScoped
@ManagedBean
public class JasperController {
	
	private List<Funcionario> funcionarios;
	private FacesContext context;
	private ServletContext servletContext;
	private String path, pathToReportPackage, caminhoRelatorio;
	private JasperPrint impressao;

	@PostConstruct
	public void init() {
		this.path = this.getClass().getClassLoader().getResource("").getPath();
		this.pathToReportPackage = this.path + "jasper/";
		gerarFuncionarios();
	}

	private void gerarFuncionarios(){
		funcionarios = new ArrayList<Funcionario>();
		Integer numeroFuncionarios = 5+(int)(6*Math.random()); 
		Integer i = 1;
		do{
			Funcionario funcionario = new Funcionario();
			funcionario.setNome("Funcionario 0"+i);
			funcionario.setMatricula("0000000"+i);
			funcionario.setTempoEmpresa(5+(long)(6*Math.random()));
			funcionarios.add(funcionario);
			i++;
		}while(i<numeroFuncionarios);
	}

	public String gerarRelatorio() throws Exception{
		context = FacesContext.getCurrentInstance();
		servletContext = (ServletContext) context.getExternalContext().getContext();
		caminhoRelatorio = this.getPathToReportPackage().concat("Funcionarios.jrxml");
		enviarPdf(funcionarios, caminhoRelatorio, "RelatorioFuncionarios.pdf");
		return null;
	}
	
	public String gerarRelatorioGrafico() throws Exception{
		context = FacesContext.getCurrentInstance();
		servletContext = (ServletContext) context.getExternalContext().getContext();
		caminhoRelatorio = this.getPathToReportPackage().concat("TempoEmpresa.jrxml");
		enviarPdf(funcionarios, caminhoRelatorio, "RelatorioFuncionarios.pdf");
		return null;
	}
	
	public void enviarPdf(List<Funcionario> funcionarios, String nomeJasper, String nomeArquivoPDF) {
        try {
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.setContentType("application/pdf");
            response.addHeader("Content-disposition", "attachment; filename=\""+nomeArquivoPDF+"\"");
            JasperReport report = JasperCompileManager.compileReport(caminhoRelatorio);
    		impressao = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(this.funcionarios));
            JasperExportManager.exportReportToPdfStream(impressao, response.getOutputStream());
            context.responseComplete();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
	
	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPathToReportPackage() {
		return pathToReportPackage;
	}

	public void setPathToReportPackage(String pathToReportPackage) {
		this.pathToReportPackage = pathToReportPackage;
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

}
