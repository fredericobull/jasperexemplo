package model;

public class Funcionario {
	private String nome;
	private String matricula;
	private Long tempoEmpresa;
	
	public Long getTempoEmpresa() {
		return tempoEmpresa;
	}
	public void setTempoEmpresa(Long tempoEmpresa) {
		this.tempoEmpresa = tempoEmpresa;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
}
